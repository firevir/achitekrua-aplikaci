﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinceznaZeMlyna.Model.Interakce
{
    public class OdhalLokaciInterakce : abstractInterakce
    {

        public OdhalLokaciInterakce(Mistnost odhal)
        {
            TypMetody = MetodyEnum.ODHAL_LOKACI;
            Obsah = new object[] { odhal };
        }
    }
}
