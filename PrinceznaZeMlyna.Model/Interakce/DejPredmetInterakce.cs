﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinceznaZeMlyna.Model.Interakce
{
    public class DejPredmetInterakce : abstractInterakce
    {
        public DejPredmetInterakce(Predmet dejPredmet)
        {
            TypMetody = MetodyEnum.DEJ_ITEM;
            Obsah = new object[] { dejPredmet };
        }
    }
}
