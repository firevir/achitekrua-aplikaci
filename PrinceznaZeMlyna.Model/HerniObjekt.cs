﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.Model.Interakce;
namespace PrinceznaZeMlyna.Model
{
    abstract public class HerniObjekt
    {
        public string Nazev { get; set; }
        public string Popis { get; set; }

        private Dictionary<string, object> _herniInformace;
        public Dictionary<string, object> HerniInformace { get { return _herniInformace; } }
        public HerniObjekt()
        {
            _herniInformace = new Dictionary<string, object>();
            interactDelegate += AddInteractCounter;
        }

        public override string ToString()
        {
            return $"{Nazev} ({Popis})";
        }

        public abstract string PopisSe();

        public delegate List<abstractInterakce> InteractHandler(HerniObjekt caller);
        public InteractHandler interactDelegate;
        public List<abstractInterakce> UseInteract()
        {
            List<abstractInterakce> results = new List<abstractInterakce>();

            foreach (InteractHandler interactDel in interactDelegate.GetInvocationList())
            {
                List<abstractInterakce> newIW = interactDel(this);
                if (newIW != null)
                {
                    results.AddRange(newIW);
                }
            }
            return results;
        }
        public int InteractCounter { get; set; }


        private static List<abstractInterakce> AddInteractCounter(HerniObjekt caller)
        {
            if (!caller.HerniInformace.ContainsKey("InteractCounter"))
            {
                caller.HerniInformace["InteractCounter"] = 0;
            }
            else
            {
                int i = (int)caller.HerniInformace["InteractCounter"];
                caller.HerniInformace["InteractCounter"] = ++i;
            }
            return null;
        }
    }
}
