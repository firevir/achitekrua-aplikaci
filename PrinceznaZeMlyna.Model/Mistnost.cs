﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrinceznaZeMlyna.Model
{
    public class Mistnost : HerniObjekt
    {
        public List<Mistnost> SoudedniMistnosti { get; set; }
        public List<Postava> Postavy { get; set; }
        public List<Predmet> Predmety { get; set; }

        public Mistnost()
        {
            SoudedniMistnosti = new List<Mistnost>();
            Postavy = new List<Postava>();
            Predmety = new List<Predmet>();
        }

        public override string ToString()
        {
            return this.Nazev;
        }

        public override string PopisSe()
        {
            string pomS = "";
            foreach (Mistnost item in SoudedniMistnosti)
            {
                pomS += $"{ item.Nazev }, ";
            }
            string pomP = "";
            foreach (Postava item in Postavy)
            {
                pomP += $"{ item.Nazev }, ";
            }
            string pomPr = "";
            foreach (Predmet item in Predmety)
            {
                pomPr += $"{ item.Nazev }, ";
            }
            return $"{this.Nazev} \n {this.Popis}\n\n{Properties.Resources.NeighboursWith} {pomS}\n{Properties.Resources.CharactersLocated}: {pomP}\n{Properties.Resources.ItemsLocated}: {pomPr}";
        }
    }
}
