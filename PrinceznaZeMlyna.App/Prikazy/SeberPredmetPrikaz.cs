﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.Model;
using PrinceznaZeMlyna.Model.Interakce;
namespace PrinceznaZeMlyna.App.Prikazy
{
    public class SeberPredmetPrikaz : iPrikaz
    {
        private Predmet _predmet;

        public SeberPredmetPrikaz(Predmet predmet)
        {
            _predmet = predmet;
        }

        public bool ProvedPrikaz()
        {
            if (Hra.AktualniMistnost.Predmety.Contains(_predmet))
            {
                VykonejMetodu.ProvedInteract(new ZpravaInterakce($"{Model.Properties.Resources.TakeItemLabel}: {_predmet.Nazev}"));
                VykonejMetodu.ProvedInteract(new DejPredmetInterakce(_predmet));
                Hra.AktualniMistnost.Predmety.Remove(_predmet);
                List<abstractInterakce> returns = _predmet.UseInteract();
                foreach (abstractInterakce interact in returns)
                {
                    VykonejMetodu.ProvedInteract(interact);
                }
                return true;
            }
            return false;
        }
    }
}
