﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.Model;
using PrinceznaZeMlyna.Model.Interakce;
namespace PrinceznaZeMlyna.App.Prikazy
{
    public class MluvPrikaz : iPrikaz
    {
        private Postava _mluvS;
        public MluvPrikaz(Postava mluvS)
        {
            _mluvS = mluvS;
        }

        public bool ProvedPrikaz()
        {
            if (Hra.AktualniMistnost.Postavy.Contains(_mluvS))
            {
                List<abstractInterakce> returns = _mluvS.UseInteract();
                foreach (abstractInterakce interact in returns)
                {
                    VykonejMetodu.ProvedInteract(interact);
                }
                return true;
            }
            return false;
        }
    }
}
