﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.Model;
namespace PrinceznaZeMlyna.App
{
    public interface iView
    {
        void Show(string s);
    }
}
