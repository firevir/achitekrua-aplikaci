﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.Model;
using PrinceznaZeMlyna.App.Metody;
using System.Reflection;
using PrinceznaZeMlyna.Model.Interakce;
namespace PrinceznaZeMlyna.App
{
    class VykonejMetodu
    {
        public static void ProvedInteract(abstractInterakce metoda)
        {
            var types = AppDomain.CurrentDomain.GetAssemblies()
             .SelectMany(s => s.GetTypes())
             .Where(p => typeof(abstractMetoda).IsAssignableFrom(p) && typeof(abstractMetoda) != p);
            foreach (Type type in types)
            {
                if ((MetodyEnum)type.GetProperty("_metoda").GetValue(null) == metoda.TypMetody)
                {
                    type.GetMethod("ProvedMetodu").Invoke(null, new object[] { metoda.Obsah });
                }
            }

            /* switch (metoda.TypMetody)
             {
                 case MetodyEnum.DEJ_ITEM:
                     Hra.PridejItem((Predmet)metoda.Obsah[0]);
                     break;
                 case MetodyEnum.ODHAL_LOKACI:
                     Hra.AktualniMistnost.SoudedniMistnosti.Add((Mistnost)metoda.Obsah[0]);
                     break;
                 case MetodyEnum.ZPRAVA:
                     Hra.View.Show((string)metoda.Obsah[0]);
                     break;
                 case MetodyEnum.JDI:
                     Hra.AktualniMistnost = (Mistnost)metoda.Obsah[0];
                     break;
             }*/
        }
    }
}
