﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.Model;
namespace PrinceznaZeMlyna.App.Metody
{
    class DejItemMetoda : abstractMetoda
    {
        private DejItemMetoda() { }
        public static new MetodyEnum _metoda
        { get { return MetodyEnum.DEJ_ITEM; } }

        public static new void ProvedMetodu(object[] obj)
        {
            Hra.PridejItem((Predmet)obj[0]);
        }
    }
}
