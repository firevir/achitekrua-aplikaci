﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.Model;
namespace PrinceznaZeMlyna.App.Metody
{
    class OdhalLokaciMetoda : abstractMetoda
    {
        private OdhalLokaciMetoda() { }
        public static new MetodyEnum _metoda
        { get { return MetodyEnum.ODHAL_LOKACI; } }

        public static new void ProvedMetodu(object[] obj)
        {
            Hra.AktualniMistnost.SoudedniMistnosti.Add((Mistnost)obj[0]);
        }
    }
}
