﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.Model;
namespace PrinceznaZeMlyna.App.Metody
{
    class ZobrazVeViewMetoda : abstractMetoda
    {
        private ZobrazVeViewMetoda() { }
        public static new MetodyEnum _metoda
        { get { return MetodyEnum.ZPRAVA; } }

        public static new void ProvedMetodu(object[] obj)
        {
            Hra.View.Show((string)obj[0]);
        }
    }
}
