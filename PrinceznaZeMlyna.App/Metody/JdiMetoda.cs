﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrinceznaZeMlyna.Model;
namespace PrinceznaZeMlyna.App.Metody
{
    class JdiMetoda : abstractMetoda
    {
        private JdiMetoda() { }
        public static new MetodyEnum _metoda
        { get { return MetodyEnum.JDI; } }

        public static new void ProvedMetodu(object[] obj)
        {
            Hra.AktualniMistnost = (Mistnost)obj[0];
        }
    }
}
